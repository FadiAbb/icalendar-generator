# Icalendar generator
Generate a .ics file with a specific number of records.

After building the main.go file `go build main.go` and starting the generated `./icalander-generator` file in the command line, you will be asked to enter a start-date and the number of records which leads to generate a icalendar file.

Generated file example (date: 2021-10-5, record: 2):

```
BEGIN:VCALENDAR
CALSCAL:GREGORIAN
PRODID:-//github.com/anders/ics
VERSION:2.0
BEGIN:VEVENT
DTSTART:20211005T010000Z
SUMMARY:Summary
END:VEVENT
BEGIN:VEVENT
DTSTART:20211005T020000Z
SUMMARY:Summary
END:VEVENT
END:VCALENDAR
```

## License

Icalendar-generator as a complete package is a free Software under GNU Affero General Public License v >= 3.0 without warranty.

The generated files are licensed under CC BY 4.0 .
